	<footer class="main-footer row no-gutters" itemscope itemtype="http://schema.org/Organization">
		<meta itemprop="name" content="<?php echo get_theme_mod( 'contact_name' ) ?>" />
		<link itemprop="url" href="<?php echo get_site_url(); ?>" />
		<div class="col-12 col-lg-4">
			<div class="footer-title font-courgette">
				Social
			</div>
			<?php if ( get_theme_mod( 'social_media_facebook_url' ) && get_theme_mod( 'social_media_facebook_handle' ) ) : ?>
			<div class="footer-item" title="Facebook">
				<a itemprop="sameAs" href="<?php echo get_theme_mod( 'social_media_facebook_url' ) ?>" target="_blank">
					<div class="icon"><i class="fab fa-facebook-square"></i></div>
					<div class="text"><?php echo get_theme_mod( 'social_media_facebook_handle' ) ?></div>
				</a>
			</div>
			<?php endif; ?>
			<?php if ( get_theme_mod( 'social_media_twitter_url' ) && get_theme_mod( 'social_media_twitter_handle' ) ) : ?>
			<div class="footer-item" title="Twitter">
				<a itemprop="sameAs" href="<?php echo get_theme_mod( 'social_media_twitter_url' ) ?>" target="_blank">
					<div class="icon"><i class="fab fa-twitter-square"></i></div>
					<div class="text"><?php echo get_theme_mod( 'social_media_twitter_handle' ) ?></div>
				</a>
			</div>
			<?php endif; ?>
			<?php if ( get_theme_mod( 'social_media_google_plus_url' ) && get_theme_mod( 'social_media_google_plus_handle' ) ) : ?>
			<div class="footer-item" title="Google+">
				<a itemprop="sameAs" href="<?php echo get_theme_mod( 'social_media_google_plus_url' ) ?>" target="_blank">
					<div class="icon"><i class="fab fa-google-plus-square"></i></div>
					<div class="text"><?php echo get_theme_mod( 'social_media_google_plus_handle' ) ?></div>
				</a>
			</div>
			<?php endif; ?>
			<?php if ( get_theme_mod( 'social_media_youtube_url' ) && get_theme_mod( 'social_media_youtube_handle' ) ) : ?>
			<div class="footer-item" title="Youtube">
				<a itemprop="sameAs" href="<?php echo get_theme_mod( 'social_media_youtube_url' ) ?>" target="_blank">
					<div class="icon"><i class="fab fa-youtube"></i></div>
					<div class="text"><?php echo get_theme_mod( 'social_media_youtube_handle' ) ?></div>
				</a>
			</div>
			<?php endif; ?>
		</div>
		<div class="col-12 col-lg-4">
			<div class="footer-title font-courgette">
				Contact Us
			</div>
			<div class="footer-item">
				<a itemprop="telephone" content="<?php echo c_strip_phone_number( get_theme_mod( 'contact_telephone' ) )?>" href="tel:<?php echo c_strip_phone_number( get_theme_mod( 'contact_telephone' ) ) ?>">
					<div class="icon"><i class="fas fa-phone fa-fw"></i></div>
					<div class="text"><?php echo get_theme_mod( 'contact_telephone' ) ?></div>
				</a>
			</div>
			<div class="footer-item">
				<a href="/#">
				<div class="icon"><i class="fas fa-envelope fa-fw"></i></div>
				<div itemprop="email" class="text"><?php echo get_theme_mod( 'contact_email' ) ?></div>
				</a>
			</div>
			<div class="footer-item" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<a href="https://www.google.com/maps/search/?api=1&query=<?php echo get_theme_mod( 'contact_street_address' ) ?> <?php echo get_theme_mod( 'contact_address_locality' ) ?> <?php echo get_theme_mod( 'contact_address_region' ) ?> <?php echo get_theme_mod( 'contact_address_postal_code' ) ?>"  target="_blank">
					<div class="icon"><i class="fas fa-map fa-fw"></i></div>
					<div class="text">
						<span itemprop="streetAddress"><?php echo get_theme_mod( 'contact_street_address' ) ?></span><br/>
						<span itemprop="addressLocality"><?php echo get_theme_mod( 'contact_address_locality' ) ?></span>, <span itemprop="addressRegion"><?php echo get_theme_mod( 'contact_address_region' ) ?></span> <span itemprop="postalCode"><?php echo get_theme_mod( 'contact_address_postal_code'  ) ?></span><br/>
					</div>
				</a>
			</div>
		</div>
		<div class="col-12 col-lg-4">
			<div class="footer-title font-courgette">
				Site
			</div>
			<div class="footer-item" title="title">
				<a href="/#">
					<div class="icon"></div>
					<div class="text">LINK</div>
				</a>
			</div>
			<div class="footer-item" title="title">
				<a href="/#">
					<div class="icon"></div>
					<div class="text">LINK</div>
				</a>
			</div>
			<div class="footer-item" title="title">
				<a href="/#">
					<div class="icon"></div>
					<div class="text">LINK</div>
				</a>
			</div>
			<div class="footer-item" title="title">
				<a href="/#">
					<div class="icon"></div>
					<div class="text">LINK</div>
				</a>
			</div>
		</div>
	</footer>
	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "<?php echo get_theme_mod( 'contact_name' ) ?>",
		"url": "<?php echo get_site_url(); ?>",
		"logo": "<?php echo get_site_icon_url(); ?>",
		"contactPoint": [
			{ "@type": "ContactPoint",
			"telephone": "<?php echo c_strip_phone_number( get_theme_mod( 'contact_telephone' ) )?>",
			"email": "<?php echo get_theme_mod( 'contact_email' ) ?>",
			"contactType": "customer service"
			}
		],
		"email": "<?php echo get_theme_mod( 'contact_email' ) ?>",
		"telephone": "<?php echo c_strip_phone_number( get_theme_mod( 'contact_telephone' ) )?>",
		"address": {
			"@type": "PostalAddress",
			"streetAddress": "<?php echo get_theme_mod( 'contact_street_address' ) ?>",
			"addressLocality": "<?php echo get_theme_mod( 'contact_address_locality' ) ?>",
			"addressRegion": "<?php echo get_theme_mod( 'contact_address_region' ) ?>",
			"postalCode": "<?php echo get_theme_mod( 'contact_address_postal_code' ) ?>"
		},
		"sameAs":[
			<?php if ( get_theme_mod( 'social_media_facebook_url' ) ) {
				echo '"'.get_theme_mod( 'social_media_facebook_url' ).'",';
			} ?>
			<?php if ( get_theme_mod( 'social_media_twitter_url' ) ) {
				echo '"'.get_theme_mod( 'social_media_twitter_url' ).'",';
			} ?>
			<?php if ( get_theme_mod( 'social_media_google_plus_url' ) ) {
				echo '"'.get_theme_mod( 'social_media_google_plus_url' ).'",';
			} ?>
			<?php if ( get_theme_mod( 'social_media_youtube_url' ) ) {
				echo '"'.get_theme_mod( 'social_media_youtube_url' ).'"';
			} ?>
		]
	}
	</script>
	<?php wp_footer(); ?>
</body>
</html>