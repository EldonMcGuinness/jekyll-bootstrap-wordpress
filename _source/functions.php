---
---
<?php
global $post;
setup_postdata( $post );

function c_strip_phone_number( $tel = null ){
	$valid_chars = array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+');

	if ( empty( $tel ) ){
		return null;

	}else{
		$formatted_tel = '';

		foreach( str_split( $tel ) as $digit ){
			if ( in_array( $digit, $valid_chars, true ) ){
				$formatted_tel .= $digit;
			}
		}

		return $formatted_tel;
	}
}

function c_the_featured_image( $post = 0 ) {
	$post = get_post( $post );
	$post_thumbnail_id = get_post_thumbnail_id( $post );
	return ( $post_thumbnail_id ) ? wp_get_attachment_image_url( $post_thumbnail_id, 'post-thumbnail' ) : null;
}

function c_the_social_media_image() {
	$social_media_image = null;

	if ( ! empty( get_theme_mod( 'social_media_share_image' ) ) || ! empty( c_the_featured_image() ) ) {
		$social_media_image = ( ! empty( c_the_featured_image() ) ) ? c_the_featured_image() : get_theme_mod( 'social_media_share_image' );
	}

	return $social_media_image;
}

function c_the_excerpt(){
	return (get_the_excerpt()) ? strip_tags(get_the_excerpt()) : get_bloginfo( 'description' );
}

function {{ site.wp.namespace }}_scripts() {

	// JS
	wp_deregister_script('jquery');
	{% assign js = '' | split: '' %}{% for script in site.bootstrap_includes.js %}{% assign js = js | push: script %}{% endfor %}{% for script in site.fontawesome_includes.js %}{% assign js = js | push: script %}{% endfor %}
	{% for include in js %}{% if include.deps.size > 0 %}{% assign deps = include.deps | join: "', '" | prepend: "'" | append: "'" %}{% else %}{% assign deps = nil %}{% endif %}wp_register_script( '{{ include.handle }}', '{{ include.src }}', array({{ deps }}), '{{ include.ver }}', {{ include.footer }} );
	{% endfor %}
	{% for include in js %}wp_enqueue_script( '{{ include.handle }}' );
	{% endfor %}

	// CSS
	{% assign css = '' | split: '' %}{% for script in site.bootstrap_includes.css %}{% assign css = css | push: script %}{% endfor %}
	{% for include in css %}{% if include.deps.size > 0 %}{% assign deps = include.deps | join: "', '" | prepend: "'" | append: "'" %}{% else %}{% assign deps = nil %}{% endif %}wp_register_style( '{{ include.handle }}', '{{ include.src }}', array({{ deps }}), '{{ include.ver }}', '{{ include.media }}' );
	{% endfor %}
	{% for include in css %}wp_enqueue_style( '{{ include.handle }}' );
	{% endfor %}
}
add_action( 'wp_enqueue_scripts', '{{ site.wp.namespace }}_scripts' ); 

function {{ site.wp.namespace }}_site_verification_section( $wp_customize ) {
	$wp_customize->add_section( 'site_verification_section', array(
		'title'    => __( 'Site Verification Tokens', '{{ site.wp.namespace }}' ),
		'priority' => 103
	));
	
	$wp_customize->add_setting( 'site_verification_google', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'site_verification_bing', array(
		'default' => ''
	));
	
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'site_verification_google', array(
		'label'       => __( 'Google Verification Tokens', '{{ site.wp.namespace }}' ),
		'description' => __( '', '{{ site.wp.namespace }}' ),
		'section'     => 'site_verification_section',
		'settings'    => 'site_verification_google'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'site_verification_bing', array(
		'label'       => __( 'Bing Verification Tokens', '{{ site.wp.namespace }}' ),
		'description' => __( '', '{{ site.wp.namespace }}' ),
		'section'     => 'site_verification_section',
		'settings'    => 'site_verification_bing'
	)));
}
add_action( 'customize_register', '{{ site.wp.namespace }}_site_verification_section' );

function {{ site.wp.namespace }}_social_media_section( $wp_customize ) {
	$wp_customize->add_section( 'social_media_section', array(
		'title'    => __( 'Social Media', '{{ site.wp.namespace }}' ),
		'priority' => 102
	));
	
	$wp_customize->add_setting( 'social_media_share_image', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'social_media_facebook_handle', array(
		'default' => '{{ site.social_media.facebook.handle }}'
	));

	$wp_customize->add_setting( 'social_media_facebook_url', array(
		'default' => '{{ site.social_media.facebook.url }}'
	));

	$wp_customize->add_setting( 'social_media_twitter_handle', array(
		'default' => '{{ site.social_media.twitter.handle }}'
	));

	$wp_customize->add_setting( 'social_media_twitter_url', array(
		'default' => '{{ site.social_media.twitter.url }}'
	));

	$wp_customize->add_setting( 'social_media_youtube_handle', array(
		'default' => '{{ site.social_media.youtube.handle }}'
	));

	$wp_customize->add_setting( 'social_media_youtube_url', array(
		'default' => '{{ site.social_media.youtube.url }}'
	));

	$wp_customize->add_setting( 'social_media_google_plus_handle', array(
		'default' => '{{ site.social_media.google_plus.handle }}'
	));

	$wp_customize->add_setting( 'social_media_google_plus_url', array(
		'default' => '{{ site.social_media.google_plus.url }}'
	));

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'social_media_share_image', array(
		'label'       => __( 'Social Media Share Image', '{{ site.wp.namespace }}' ),
		'description' => __( 'Default image for social media sharing<br/>( 1200px &times; 628px OR 1.91:1 )', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_share_image'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_facebook_handle', array(
		'label'       => __( 'Facebook Handle', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Facebook page handle (@yourprofile)', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_facebook_handle'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_facebook_url', array(
		'label'       => __( 'Facebook URL', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Facebook profile URL', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_facebook_url'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_twitter_handle', array(
		'label'       => __( 'Twitter Handle', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Twitter profile handle (@yourprofile)', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_twitter_handle'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_twitter_url', array(
		'label'       => __( 'Twitter URL', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Twitter profile URL', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_twitter_url'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_youtube_handle', array(
		'label'       => __( 'Youtube Handle', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Youtube page handle (@yourprofile)', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_youtube_handle'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_youtube_url', array(
		'label'       => __( 'Youtube URL', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Youtube page URL', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_youtube_url'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_google_plus_handle', array(
		'label'       => __( 'Google Plus Handle', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Google+ page handle (+yourprofile)', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_google_plus_handle'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'social_media_google_plus_url', array(
		'label'       => __( 'Google Plus URL', '{{ site.wp.namespace }}' ),
		'description' => __( 'Specify your Google+ page URL', '{{ site.wp.namespace }}' ),
		'section'     => 'social_media_section',
		'settings'    => 'social_media_google_plus_url'
	)));
}
add_action( 'customize_register', '{{ site.wp.namespace }}_social_media_section' );

function {{ site.wp.namespace }}_site_logo_section( $wp_customize ) {
	$wp_customize->add_section( 'site_logo_section', array(
		'title'    => __( 'Site Logo', '{{ site.wp.namespace }}' ),
		'priority' => 101
	));
	
	$wp_customize->add_setting( 'site_logo', array(
		'default' => ''
	));
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'site_logo', array(
		'label'       => __( 'Site Logo', '{{ site.wp.namespace }}' ),
		'description' => __( 'Choose the image to be displayed on the homepage', '{{ site.wp.namespace }}' ),
		'section'     => 'site_logo_section',
		'settings'    => 'site_logo'
	)));
}
add_action( 'customize_register', '{{ site.wp.namespace }}_site_logo_section' );

function {{ site.wp.namespace }}_contact_info_section( $wp_customize ) {
	$wp_customize->add_section( 'contact_info_section', array(
		'title' => __('Contact Info', '{{ site.wp.namespace }}'),
		'priority' => 100
	));
	
	$wp_customize->add_setting( 'contact_telephone', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'contact_name', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'contact_email', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'contact_street_address', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'contact_address_locality', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'contact_address_region', array(
		'default' => ''
	));

	$wp_customize->add_setting( 'contact_address_postal_code', array(
		'default' => ''
	));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_name', array(
		'label'       => __( 'Entity Name', '{{ site.wp.namespace }}' ),
		'description' => __( 'Your company name', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_name'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_telephone', array(
		'label'       => __( 'Telephone', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_telephone'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_email', array(
		'label'       => __( 'Email', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_email'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_street_address', array(
		'label'       => __( 'Street Address', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_street_address'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_address_locality', array(
		'label'       => __( 'Locality (City)', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_address_locality'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_address_region', array(
		'label'       => __( 'Region (State)', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_address_region'
	)));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_address_postal_code', array(
		'label'       => __( 'Postal Code', '{{ site.wp.namespace }}' ),
		'section'     => 'contact_info_section',
		'settings'    => 'contact_address_postal_code'
	)));
}
add_action( 'customize_register', '{{ site.wp.namespace }}_contact_info_section' );
?>