---
---
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="msapplication-TileColor" content="#002d41">
<?php if ( ! empty( get_site_icon_url() ) ) :?>
	<meta name="msapplication-TileImage" content="<?php echo get_site_icon_url(); ?>">
<?php endif; ?>
	<meta name="theme-color" content="#002d41">
	<meta name="twitter:card" content="summary_large_image" >
<?php if ( ! empty( get_theme_mod( 'social_media_twitter_handle' ) ) ) :?>
	<meta name="twitter:site" content="<?php echo get_theme_mod( 'social_media_twitter_handle' ) ?>" >
<?php endif; ?>
	<meta name="twitter:title" content="<?php echo the_title(); ?>" >
	<meta name="twitter:description" content="<?php echo c_the_excerpt(); ?>" >
<?php if ( c_the_social_media_image() ) : ?>
	<meta name="twitter:image" content="<?php echo c_the_social_media_image(); ?>" >
<?php endif; ?>
	<meta property="og:title" content="<?php echo the_title(); ?>" >
	<meta property="og:type" content="website" >
	<meta property="og:url" content="<?php echo get_the_permalink(); ?>" >
	<meta property="og:description" content="<?php echo c_the_excerpt(); ?>" >
	<meta property="og:site_name" content="<?php echo get_bloginfo( 'name' ); ?>" >
<?php if ( ! empty( get_theme_mod( 'social_media_share_image' ) ) || ! empty( c_the_featured_image() ) ) : ?>
	<meta property="og:image" content="<?php echo c_the_social_media_image(); ?>" >
	<meta property="og:image:url" content="<?php echo c_the_social_media_image(); ?>" >
<?php endif; ?>
<?php if ( ! empty( get_theme_mod( 'site_verification_google' ) ) ) :?>
	<meta name="google-site-verification" content="<?php echo get_theme_mod( 'site_verification_google' ); ?>">
<?php endif; ?>
<?php if ( ! empty( get_theme_mod( 'site_verification_bing' ) ) ) :?>
	<meta name="msvalidate.01" content="<?php echo get_theme_mod( 'site_verification_bing' ); ?>">
<?php endif; ?>
	<meta name="description" content="<?php echo c_the_excerpt(); ?>">
	<title><?php echo the_title(); ?></title>
	<link rel="alternate" type="application/rss+xml" title="RSS Feed for <? echo get_bloginfo('name')?>" href="<? echo bloginfo('rss2_url')?>">
	<link rel="alternate" type="application/rss+xml" title="RSS Feed for <? echo get_bloginfo('name')?>" href="<? echo bloginfo('atom_url')?>">
	<link href="https://fonts.googleapis.com/css?family={{ site.google_fonts | join: '|' | replace: ' ', '+'}}" rel="stylesheet">
	<link rel="stylesheet" href="<? echo get_bloginfo('template_directory'); ?>/style.css" >
	<?php wp_head();?>
</head>
<body>